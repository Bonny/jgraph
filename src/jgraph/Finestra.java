package jgraph;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Finestra extends JFrame {

   private final String[] metodi = { /* metodi a scelta dell'utente */
      "Depth-first search DFS", "breadth-first search BFS"
   };
   private Graphics2D g;
   private JPanel canvas;
   private JComboBox cmbNodi, cmbMetodo;
   private JButton btnInit, btnStart;
   private final int _WIDTH = 700, _HEIGHT = 600, TIMESTAMP = 1200, DIM = 15;
   private int i, j, root = 0;
   private boolean stato = false; /* indica se il programma è in esecuzione */

   private boolean[] visitato;
   private final Nodo[] nodi = { /* punti del piano che indicano i nodi del grafo */
      new Nodo(200, 40), new Nodo(375, 40),
      new Nodo(100, 120), new Nodo(300, 120), new Nodo(500, 120),
      new Nodo(200, 200), new Nodo(375, 200),
      new Nodo(100, 280), new Nodo(300, 280), new Nodo(500, 280),
      new Nodo(200, 370), new Nodo(375, 370),
      new Nodo(100, 450), new Nodo(300, 450), new Nodo(500, 450),};
   private int[][] graph = { /* rappresentazione del grafo mediante matrice di adiacenza */
      /*      0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 */
      /* 0 */{0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      /* 1*/ {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      /* 2*/ {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      /* 3*/ {1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
      /* 4*/ {0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
      /* 5*/ {1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
      /* 6*/ {0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0},
      /* 7*/ {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0},
      /* 8*/ {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0},
      /* 9*/ {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1},
      /*10*/ {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
      /*11*/ {0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0},
      /*12*/ {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
      /*13*/ {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0},
      /*14*/ {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},};

   public Finestra() {
      super("JGraph");
      visitato = new boolean[DIM];
      initComponents();
      this.setResizable(false);
      this.setVisible(true);
      this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      Dimension dimSchermo = Toolkit.getDefaultToolkit().getScreenSize();
      this.setBounds((int) (dimSchermo.getWidth() / 2) - _WIDTH / 2, (int) (dimSchermo.getHeight() / 2) - _HEIGHT / 2, _WIDTH, _HEIGHT);

      /* listener pulsante di inizializzazione */
      btnInit.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (!stato) {
               drawGraph();
               stato = true;
            }
         }
      });
      /* listener pulsante start */
      btnStart.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (stato) {

               for (i = 0; i < visitato.length; i++) {
                  visitato[i] = false;
               }
               switch (cmbMetodo.getSelectedIndex()) {

                  case 0:
                     Coda c = new Coda();
                     DFS(graph, root, visitato, c);
                     ArrayList l = c.get();
                     j = 10;
                     for (Iterator<Integer> it = l.iterator(); it.hasNext(); j += 40) {
                        i = it.next();
                        g.drawString(" > " + i, j, 510);
                     }
                     break;
                  case 1:
                     System.out.println("\nBFS");
                     g.setColor(Color.BLACK);
                     g.drawString("POP", 590, 25);
                     GeneralPath trx = new GeneralPath();
                     trx.moveTo(560, 5);
                     trx.lineTo(570, 20);
                     trx.lineTo(550, 20);
                     trx.closePath();
                     g.fill(trx);
                     g.fillRect(555, 20, 10, 10);
                     g.drawString("PUSH", 590, 485);
                     GeneralPath tr = new GeneralPath();
                     tr.moveTo(560, 465);
                     tr.lineTo(570, 480);
                     tr.lineTo(550, 480);
                     tr.closePath();
                     g.fill(tr);
                     g.fillRect(555, 480, 10, 10);
                     BFS(graph, root, visitato);
                     break;
               }
               stato = false;
            }
         }
      });
      /* listener combobox per scelta nodo root */
      cmbNodi.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (stato) {
               drawNodo(root, new Color(134, 255, 82), g);
               root = cmbNodi.getSelectedIndex();
               drawNodo(root, Color.ORANGE, g);
            }
         }
      });
      cmbMetodo.addActionListener(new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            if (stato) {
               drawNodo(root, new Color(134, 255, 82), g);
               root = cmbNodi.getSelectedIndex();
               drawNodo(root, Color.ORANGE, g);
            }
         }
      });
   }

   /* disegna nel canvas il grafo */
   private void drawGraph() {
      g = (Graphics2D) canvas.getGraphics();
      canvas.update(g);
      g.setColor(Color.BLACK);
      g.setFont(new Font("Arial", Font.PLAIN, 18));

      for (i = 0; i < graph.length; i++) {
         for (j = 0; j < graph.length; j++) {
            /* se esiste un arco tra il nodo i e il nodo j disegno l'arco */
            if (graph[i][j] == 1) {
               g.drawLine(nodi[i].x, nodi[i].y, nodi[j].x, nodi[j].y);
            }
         }
      }
      for (i = 0; i < nodi.length; i++) {
         drawNodo(i, new Color(134, 255, 82), g);
      }
      drawNodo(root, new Color(134, 255, 82), g);
      root = cmbNodi.getSelectedIndex();
      drawNodo(root, Color.ORANGE, g);
   }

   /* disegna un nodo nd di colore c */
   public void drawNodo(int nd, Color c, Graphics2D gg) {
      gg.setColor(c);
      gg.fill(nodi[nd].getArea());
      gg.setColor(Color.BLACK);
      gg.draw(nodi[nd].getBordo());
      gg.drawString(String.valueOf(nd), nodi[nd].x - 5, nodi[nd].y + 5);
   }

   public void drawCoda(Coda c, Graphics2D gg) {
      int k = 50, obj;
      gg.setColor(Color.white);
      gg.fillRect(550, 50, 150, 400);
      ArrayList<Integer> l = c.get();
      for (Iterator<Integer> it = l.iterator(); it.hasNext(); k += 50) {
         obj = it.next();
         gg.setColor(Color.YELLOW);
         gg.fillRect(550, k, 110, 50);
         gg.setColor(Color.BLACK);
         gg.drawRect(550, k, 110, 50);
         gg.drawString(String.valueOf(obj), 600, k + 27);
      }
      try {
         Thread.sleep(900);
      } catch (InterruptedException ex) {
         System.err.println("Errore durante l'operazione di Coda BFS");
      }
   }
   /* inizzializza le componenti grafiche del programma */

   private void initComponents() {

      canvas = new JPanel();
      cmbNodi = new JComboBox();
      cmbMetodo = new JComboBox();
      btnInit = new JButton("Init");
      btnStart = new JButton("Start");

      for (i = 0; i < DIM; i++) {
         cmbNodi.addItem(i);
      }
      for (i = 0; i < metodi.length; i++) {
         cmbMetodo.addItem(metodi[i]);
      }

      JPanel pnlSud = new JPanel();
      pnlSud.setLayout(new FlowLayout());
      pnlSud.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
      pnlSud.add(new JLabel("Select Root"));
      pnlSud.add(cmbNodi);
      pnlSud.add(cmbMetodo);
      pnlSud.add(btnInit);
      pnlSud.add(btnStart);

      canvas.setBackground(Color.white);
      this.getContentPane().add("Center", canvas);
      this.getContentPane().add("South", pnlSud);
   }

   private void BFS(int[][] G, int r, boolean[] visitati) {
      int u, v, k = 10;
      Coda c = new Coda();
      c.push(r);
      drawCoda(c, g);
      while (!c.isEmpty()) {/* fin quando la coda non è vuota */
         u = c.pop();/* estraggo un nodo u dalla coda */
         visitati[u] = true;/* marco u come visitato */
         drawNodo(u, Color.RED, g);
         g.drawString(" > " + u, k += 40, 510);
         //System.out.print(u + "\t");
         try {
            Thread.sleep(TIMESTAMP);
         } catch (InterruptedException ex) {
            System.err.println("Errore durante l'operazione di BFS");
         }
         drawNodo(u, new Color(0, 210, 210), g);
         System.out.print(u + "\t");
         drawCoda(c, g);
         for (v = 0; v < G.length; v++) {/* per ogni nodo v adiacente a u */
            if (G[u][v] == 1) {
               /* se non è stato visitato e non è in coda */
               if (!visitati[v] && !c.appartiene(v)) {
                  c.push(v);
                  drawCoda(c, g);
               }
            }
         }
      }
   }

   private void DFS(int[][] G, int u, boolean[] visitati, Coda c) {

      int v;
      visitati[u] = true;/* marco il nodo u come visitato */
      c.push(u);
      drawNodo(u, Color.RED, g);
      System.out.print(u + "\t");
      try {
         Thread.sleep(TIMESTAMP);
      } catch (InterruptedException ex) {
         System.err.println("Errore durante l'operazione di DFS");
      }
      drawNodo(u, new Color(0, 210, 210), g);
      /* per ogni arco uscente visito il i-esimo nodo adiacente */
      for (v = 0; v < G.length; v++) {
         if (G[u][v] == 1) {
            if (!visitati[v]) {/* se non è stato visitato */
               DFS(G, v, visitati, c);/* invoco DFS sil nodo */
            }
         }
      }

   }
}
