/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jgraph;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author bonny
 */
public class Coda {

   private ArrayList<Integer> elementi;

   public Coda() {
      elementi = new ArrayList<>();
   }

   public void push(int o) {
      elementi.add(o);
   }

   public int pop() {

      int el = -1;
      int size = elementi.size();

      if (size > 0) {
         el = elementi.get(0);
         elementi.remove(0);
      }

      return el;
   }

   public boolean isEmpty() {
      return ((elementi.size() > 0) ? false : true);
   }

   public boolean appartiene(int x) {
      boolean flag = false;
      for (Iterator<Integer> it = elementi.iterator(); it.hasNext() && !flag;) {
         int o = it.next();
         if (o == x) {
            flag = true;
         }
      }
      return flag;
   }

   public ArrayList<Integer> get() {
      return elementi;
   }
}
