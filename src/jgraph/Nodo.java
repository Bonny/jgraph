package jgraph;

import java.awt.Point;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

public class Nodo extends Point {

  private final int d = 50;

  public Nodo() {
    super();
  }

  public Nodo(int x, int y) {
    super(x, y);
  }

  public Area getArea() {
    return new Area(new Ellipse2D.Double(this.getX() - d / 2, this.getY() - d / 2, d, d));
  }

  public Area getBordo() {
    return new Area(new Ellipse2D.Double(this.getX() - d / 2, this.getY() - d / 2, d - 1, d - 1));
  }
}
